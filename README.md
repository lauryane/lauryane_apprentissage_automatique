Le dossier "doc" contient le rapport pour ce devoir.

Le dossier "data" contient le corpus d'origine scrappé préalablement, ainsi qu'un script qui découpe ce corpus en quatre fichiers txt : x_train et x_test qui contiennent le texte sans ponctuations ni sauts de lignes, et y_train et y_test qui contiennent pour chaque mot des fichiers x_train et x_test le label de ponctuation associé.

Le dossier "modeles" contient deux notebooks : modele1, qui donne en entrée chaque token séparément, et modele2 qui donne en entrée des fenêtres de phrases. Il contient aussi un fichier txt "resultat_test" qui représente le contenu du fichier x_test reponctué par le modèle 1.