import re
import string

# on ouvre le corpus et les fichiers x_train, x_test, y_train et y_test que l'on va compléter
with open("brassens_all.txt", encoding="UTF-8") as f, open("x_train.txt", "w", encoding="UTF-8") as xtrain, open("x_test.txt", "w", encoding="UTF-8") as xtest, open("y_train.txt", "w", encoding="UTF-8") as ytrain, open("y_test.txt", "w", encoding="UTF-8") as ytest:
    count=0
    # on lit le fichier ligne par ligne
    for ligne in f.readlines():
        count +=1
        # on remplace les sauts de ligne par un caractère qui n'est pas présent dans le corpus
        ligne = re.sub("\n", " @", ligne)
        ligne = re.sub(" +", " ", ligne)
        # on supprime toutes les parenthèses, accolades et crochets
        ligne = re.sub("[\[\]\(\)\{\}]", "", ligne)
        # on supprime les points de suspension de type "..." par le caractère points de suspension
        ligne = re.sub(f"\.\.\.", r"…", ligne )
        # on met des espaces autour des ponctuations
        ligne = re.sub(f"([{string.punctuation}’…“”]+)", r" \1 ", ligne )
        ligne = re.sub(" +", " ", ligne)
        # on recolle les ponctuations au mot qui les précède
        ligne = re.sub(f" ([{string.punctuation}’…“”]+)+ ", r"\1 ", ligne )
        ligne = re.sub(f" ([{string.punctuation}’…“”]+)+ ", r"\1 ", ligne )
        # on enlève les espaces en trop et les espaces en fin de ligne
        ligne = re.sub(" +", " ", ligne)
        ligne = re.sub("@ +", "@", ligne)
        # pour chaque mot de la ligne
        for mot in ligne.split(" "):
            #print(mot)
            # on supprime les ponctuations pour les écrire dans x_train et x_test
            mot_x = re.sub(f"([{string.punctuation}’…“”]+)*", "", mot)
            # les 10 000 premières lignes iront dans les fichiers train, les autres dans les fichiers test
            # (il y a 13 238 lignes au total dans le corpus !)
            if count<10000:
                xtrain.write(f"{mot_x} ")
            else:
                xtest.write(f"{mot_x} ")

            # on associe des labels en fonction des ponctuations
            if mot.endswith(",@"):
                mot_y = "virg_return"
            elif mot.endswith(".@"):
                mot_y = "point_return"
            elif mot.endswith("!@"):
                mot_y = "excl_return"
            elif mot.endswith("?@"):
                mot_y = "intr_return"
            elif mot.endswith("…@"):
                mot_y = "susp_return"
            elif mot.endswith(":@"):
                mot_y = "deuxp_return"
            elif mot.endswith("@"):
                mot_y = "return"
            elif mot.endswith("."):
                mot_y = "point"
            elif mot.endswith("…"):
                mot_y = "susp"
            elif mot.endswith("'") or mot.endswith("’"):
                mot_y = "apostrophe"
            elif mot.endswith('"') or mot.endswith("”") or mot.endswith("“"):
                mot_y = "guillemets"
            elif mot.endswith("!"):
                mot_y = "excl"
            elif mot.endswith("?"):
                mot_y = "intr"
            elif mot.endswith(":"):
                mot_y = "deuxp"
            else:
                mot_y = "none"
            # on ajoute le label dans les fichiers y_tain et _test
            if count<10000:
                ytrain.write(f"{mot_y} ")
            else:
                ytest.write(f"{mot_y} ")
